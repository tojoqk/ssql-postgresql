;;; Ssql-Postgresql --- S-expression SQL to PostgreSQL's SQL string converter
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Ssql-Postgresql.
;;;
;;; Ssql-Postgresql is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Ssql-Postgresql is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Ssql-Postgresql.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix git-download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages texinfo))

(define guile2.2-ssql-postgresql
  (package
    (name "guile2.2-ssql-postgresql")
    (version "0.1.1")
    (source (string-append (getcwd) "/ssql-postgresql-" version ".tar.gz"))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (inputs
     `(("guile" ,guile-2.2)))
    (synopsis "S-expression SQL to PostgreSQL's SQL string converter")
    (description "Ssql-Postgresql is a S-expression SQL to PostgreSQL's SQL string converter.")
    (home-page "https://gitlab.com/tojoqk/ssql-postgresql")
    (license license:gpl3+)))

(define guile-ssql-postgresql
  (package
    (name "guile-ssql-postgresql")
    (version "0.1.1")
    (source (string-append (getcwd) "/ssql-postgresql-" version ".tar.gz"))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (inputs
     `(("guile" ,guile-3.0)))
    (synopsis "S-expression SQL to PostgreSQL's SQL string converter")
    (description "Ssql-Postgresql is a S-expression SQL to PostgreSQL's SQL string converter.")
    (home-page "https://gitlab.com/tojoqk/ssql-postgresql")
    (license license:gpl3+)))

guile-ssql-postgresql
